import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CountriesService } from '../services/countries.service';
import { Country } from '../models/country';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-orders',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
  providers: [CountriesService]
})
export class OrdersComponent implements OnInit{

  countryForm: FormGroup = new FormGroup({});
  countries: Country[] = [];
  selectedCountry: Country = new Country();

  constructor(private fb: FormBuilder, private countryService: CountriesService) {

  }
  ngOnInit(): void {
    this.countryService.countries$.subscribe((countries: any) => {
      this.countries = countries;
      this.selectedCountry = this.countryService.getRandomCountry();
      console.log(this.selectedCountry);
    }
    );
    this.countryService.getCountries();
    this.countryForm = this.fb.group({
      capital: ['', Validators.required, this.capitalMatches.bind(this)],
      continent: ['', Validators.required, this.continentMatches.bind(this)],
      region: ['', Validators.required, this.regionMatches.bind(this)],
    });


  }


  capitalMatches(control: AbstractControl): Promise<boolean | null> {

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        control.value === this.selectedCountry.capital.toLocaleLowerCase ? resolve(true) : resolve(null);
      }
        , 200);
    })
  }

  continentMatches(control: AbstractControl): Promise<boolean | null> {
      
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          control.value === this.selectedCountry.continent.toLocaleLowerCase ? resolve(true) : resolve(null);
        }
          , 200);
      });
  }

  regionMatches(control: AbstractControl): Promise<boolean | null> {

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log(control.value + ' === ' + this.selectedCountry.region);
        
        control.value === this.selectedCountry.region.toLocaleLowerCase ? resolve(true) : resolve(null);
      }
        , 200);
    });
  }

  onSubmit() {
    // Show if each field of form is valid
    console.log('Capital: ' + this.countryForm.get(['capital'])?.valid);
    console.log('Continent: ' + this.countryForm.get(['continent'])?.valid);
    console.log('Region: ' + this.countryForm.get(['region'])?.valid);
  }

}
