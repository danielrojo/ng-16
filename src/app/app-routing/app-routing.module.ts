import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CountriesComponent } from '../components/countries/countries/countries.component';
import { ReactiveComponent } from '../components/forms/reactive/reactive.component';
import { sampleGuard } from '../sample.guard';
import { PokerComponent } from '../components/poker/poker/poker.component';



const routes: Routes = [
  { path: 'countries', component: CountriesComponent },
  { path: 'form', component: ReactiveComponent, canActivate: [sampleGuard]  },
  { path: 'poker', component: PokerComponent},
  { path: '', redirectTo: '/countries', pathMatch: 'full' },
  { path: 'orders', loadChildren: () => import('../quiz/quiz.module').then(m => m.QuizModule) },
  { path: '**', redirectTo: '/countries', pathMatch: 'full', data: { title: 'Page Not Found' }}	
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  declarations: [],
  providers: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
