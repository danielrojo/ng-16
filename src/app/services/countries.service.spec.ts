import { TestBed } from '@angular/core/testing';

import { CountriesService } from './countries.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Country } from '../models/country';

describe('CountriesService', () => {
  let service: CountriesService;
  let countries: Country[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [CountriesService]

    });
    service = TestBed.inject(CountriesService);

  });

  afterEach(() => {
    countries = [];
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xit('should get a list of countries from API', () => {
    service.countries$.subscribe({
      next: (data: any) => {
        countries = data;
        console.log('Countries lenght on complete: ' + countries.length);

        expect(countries.length).toBeGreaterThan(0);
      },
      error: (error: any) => {
        expect(error).toBeUndefined();
      }
    });
    service.getCountries();
    // console.log('Countries lenght: ' + service.countries.length);
    // expect(service.countries.length).toBeGreaterThan(0);
    
  });
});
