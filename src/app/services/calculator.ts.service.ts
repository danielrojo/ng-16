import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorTsService {

  constructor() { }

  sum (a: any, b: any): number {
    if (a === undefined || b === undefined) {
      throw new Error('Parameters must be defined');
    } 
    if (typeof a !== 'number' || typeof b !== 'number') {
      throw new Error('Parameters must be numbers');
    }
    return a + b;
  }
}
