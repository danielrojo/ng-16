import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, from, map, of, toArray } from 'rxjs';
import { Country } from '../models/country';

@Injectable()
export class CountriesService {

  private _message = 'Hello from CountriesService';
  private _data: any = [];
  private _countries: Country[] = [];
  private _continents = new Set<string>();
  private _regions = new Set<string>();
  private _currencies = new Set<string>();
  public countries$ = new BehaviorSubject<Country[]>(this._data);

  constructor(private http: HttpClient) { }

  get message(): string {
    return this._message;
  }

  get data(): any {
    return [...this._data];
  }

  get countries(): Country[] {
    return [...this._countries];
  }

  get continents(): string[] {
    return [...this._continents];
  }


  get regions(): string[] {
    return [...this._regions];
  }

  get currencies(): Set<string> {
    return this._currencies;
  }

  getCountries(): void {
    if (this._countries.length > 0) {
      this.countries$.next(this.countries);
      return;
    }
    let observer = {
      next: (data: any) => {
        this._data = data;
        this._data.forEach((jsonCountry: any) => {
          let country: Country = new Country(jsonCountry);
          this._countries.push(country);
          this._continents.add(country.continent);
          this._regions.add(country.region);
        }
        );
        this.countries$.next(this.countries);
      },
      error: (error: any) => {
        console.log(error);
      },
      complete: () => {
        console.log('complete');
        
      }
    };

      this.http.get('https://restcountries.com/v3.1/all').subscribe(observer);
      console.log(this._currencies);

      this.countries$.next(this.countries);
  }

  removeCountry(index: number) {
    this._countries.splice(index, 1);
    this.countries$.next(this.countries);
  }

  getRandomCountry(): Country {
    const index = Math.floor(Math.random() * this._countries.length);
    return this._countries[index];
  }
}
