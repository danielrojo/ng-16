import { TestBed } from '@angular/core/testing';

import { PokerService } from './poker.service';
import { PokerInterpreter } from '../models/poker/pokerinterpreter';
import { Card, Suit, Value } from '../models/poker/card';

describe('PokerService', () => {
  let service: PokerService;
  let playerCards: Card[];
  let dealerCards: Card[];
  let pokerInterpreter: PokerInterpreter;


  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokerService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have a deal method', () => {
    expect(service.deal).toBeTruthy();
  });

  it('should have a playerCards$ observable', () => {
    expect(service.playerCards$).toBeTruthy();
  });

  it('should check that player loses with a pair', () => {
    playerCards = [
      new Card(Value.ace, Suit.clubs),
      new Card(Value.ace, Suit.diamonds),
      new Card(Value.three, Suit.hearts),
      new Card(Value.four, Suit.hearts),
      new Card(Value.five, Suit.hearts)];
    dealerCards = [
      new Card(Value.two, Suit.clubs),
      new Card(Value.two, Suit.diamonds),
      new Card(Value.two, Suit.hearts),
      new Card(Value.three, Suit.hearts),
      new Card(Value.four, Suit.hearts)];
    pokerInterpreter = new PokerInterpreter(playerCards, dealerCards);
    expect(pokerInterpreter.playerWins()).toBe(false);
  });

  it('should check that player wins with a pair', () => {
    playerCards = [
      new Card(Value.ace, Suit.clubs),
      new Card(Value.ace, Suit.diamonds),
      new Card(Value.three, Suit.hearts),
      new Card(Value.four, Suit.hearts),
      new Card(Value.five, Suit.hearts)];
    dealerCards = [
      new Card(Value.two, Suit.clubs),
      new Card(Value.three, Suit.diamonds),
      new Card(Value.four, Suit.hearts),
      new Card(Value.five, Suit.hearts),
      new Card(Value.six, Suit.hearts)];
    pokerInterpreter = new PokerInterpreter(playerCards, dealerCards);
    expect(pokerInterpreter.playerWins()).toBe(true);
  });


});
