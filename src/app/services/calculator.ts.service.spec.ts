import { TestBed } from '@angular/core/testing';

import { CalculatorTsService } from './calculator.ts.service';

describe('CalculatorTsService', () => {
  let service: CalculatorTsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculatorTsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#sum should return 4', () => {
    expect(service.sum(2, 2)).toBe(4);
  });

  it('#sum should return Nan when undefined as parameter', () => {
    expect(() => {
      service.sum(undefined, 2);
    }
    ).toThrowError('Parameters must be defined');
  });

  it('#sum should detect when parameters are not numbers', () => {
    expect(() => {
      service.sum('a', 2);
    }
    ).toThrowError('Parameters must be numbers');
  });
});
