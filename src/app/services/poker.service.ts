import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Card } from '../models/poker/card';
import { PokerFacade } from '../models/poker/pokerfacade';

@Injectable({
  providedIn: 'root'
})
export class PokerService {

  private _pokerDeckFacade = new PokerFacade();
  private _playerCards: Card[] = [];
  private _dealerCards: Card[] = [];
  playerCards$ = new BehaviorSubject<Card[]>(this._playerCards);
  dealerCards$ = new BehaviorSubject<Card[]>(this._dealerCards);
  resolve$ = new BehaviorSubject<any>(false);

  constructor() {

  }

  deal(){
    let cards = this._pokerDeckFacade.deal();
    if (cards) {
      this._playerCards = cards[0];
      this._dealerCards = cards[1];
      this.playerCards$.next(this._playerCards);
      this.dealerCards$.next(this._dealerCards);
    }else {
      this.resolve$.next({
        playerCards: this._playerCards,
        dealerCards: this._dealerCards,
        winner: this._pokerDeckFacade.playerWins
      });
      }
    }
  }
