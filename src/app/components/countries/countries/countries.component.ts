import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Observable, OperatorFunction, debounceTime, distinctUntilChanged, filter, from, map, of, tap, toArray } from 'rxjs';
import { Country } from 'src/app/models/country';
import { CountriesService } from 'src/app/services/countries.service';
import { Order } from 'src/app/types/types';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit, OnDestroy {

  data: Country[] = [];
  filteredCountries: Country[] = [];
  message = 'hola';
  countryName = '';
  selectedOrder = 'Select an order';
  selectedContinent = 'Select a continent';
  selectedRegion = 'Select a region';
  continents: string[] = [];
  regions: string[] = [];

  model: any;

  myOrder = Order;

  myClass = [
    'btn btn-primary',
    'btn btn-secondary',
    'btn btn-secondary',
    'btn btn-secondary',
  ]

  constructor(private service: CountriesService) { }

  ngOnInit(): void {


    this.service.countries$.subscribe((countries: any) => {
      // console.log(data);
      this.data = countries;
      this.filteredCountries = this.data;
      this.continents = this.service.continents;
      this.regions = this.service.regions;

    });

    this.service.getCountries();
    console.log(this.service.regions);
    console.log(this.service.continents);
    // console.log(this.service.currencies);


  }

  ngOnDestroy(): void {
    console.log('destroy');
    // save data to local storage as cookie


  }

  orderBy(order: Order) {
    this.setPrimaryButton(order);
    switch (order) {
      case Order.population:
        this.data.sort((a, b) => {
          return b.population - a.population;
        });
        break;
      case Order.area:
        this.data.sort((a, b) => {
          return b.area - a.area;
        });
        break;
      case Order.density:
        this.data.sort((a, b) => {
          return b.density - a.density;
        });
        break;
      case Order.name:
        this.data.sort((a, b) => {
          return a.name.localeCompare(b.name);
        });
        break;
    }
  }

  handleDropdown(order: Order) {
    console.log(order);
    this.selectedOrder = Order[order];
    this.orderBy(order);
  }

  setPrimaryButton(order: Order) {
    this.myClass = this.myClass.map((cssClass, index) => {
      if (index === order) {
        return 'btn btn-primary';
      }
      return 'btn btn-secondary';
    });
  }

  sortByPopulation() {
    // sort by population
    // this.data.sort((a, b) => {
    //   return b.population - a.population;
    // });
    of(this.data).pipe(
      map((countries: any) => {
        countries.sort((a: any, b: any) => {
          return b.population - a.population;
        });
        return countries;
      })
    ).subscribe((data) => {
      this.data = data;
    }

    );
  }

  readonly getCountries = () =>
    this.data.filter((country) =>
      country.name.toLowerCase().includes(this.countryName.toLowerCase()));

  handleKey(value: string) {
    console.log(value);
    this.countryName = value;
    this.filteredCountries = this.getCountries();
  }

  removeCountry(country: Country) {
    console.log('remove country');
    const index = this.data.indexOf(country);
    this.service.removeCountry(index);
  }

  filterbyContinent(continent: string) {
    // console.log(continent);
    this.selectedContinent = continent;
    // this.filteredCountries = this.data.filter((country) => country.continent === continent);
    from(this.data).pipe(
      filter((country: any) => country.continent === continent),
      toArray()
    ).subscribe((data) => {
      this.filteredCountries = data;
    }

    );
  }

  filterbyRegion(region: string) {
    // console.log(region);
    this.selectedRegion = region;
    // this.filteredCountries = this.data.filter((country) => country.region === region);
    from(this.data).pipe(
      filter((country: any) => country.region === region),
      toArray()
    ).subscribe((data) => {
      this.filteredCountries = data;
    }

    );
  }


  search: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term) =>
        term.length < 1 ? [] : this.data.map((country) => country.name).filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10),
      ),
    );


}
