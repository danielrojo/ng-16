import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesComponent } from './countries.component';
import { CountriesService } from 'src/app/services/countries.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('CountriesComponent', () => {
  let component: CountriesComponent;
  let fixture: ComponentFixture<CountriesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CountriesComponent],
      providers: [CountriesService],
      imports: [HttpClientModule, FormsModule, NgbModule],
    });
    fixture = TestBed.createComponent(CountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#getCountries should return a list of countries', () => {
    console.log('Countries lenght: ' + component.getCountries().length);
    component.getCountries();

    expect(component.getCountries().length).toBe(0);
  });

});
