import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ReversePipe } from 'ngx-pipes';
import { Country } from 'src/app/models/country';

@Component({
  selector: 'country-card',
  templateUrl: './country-card.component.html',
  styleUrls: ['./country-card.component.scss'],
  providers: [ReversePipe]
})
export class CountryCardComponent {

  @Input() data: Country = new Country();
  @Output() onRemove = new EventEmitter<Country>();

  removeCountry() {
    console.log('remove country');
    this.onRemove.emit(this.data);
  }
}
