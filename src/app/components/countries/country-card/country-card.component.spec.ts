import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCardComponent } from './country-card.component';
import { ReversePipe } from 'ngx-pipes';
import { PopulationPipe } from 'src/app/pipes/population.pipe';

describe('CountryCardComponent', () => {
  let component: CountryCardComponent;
  let fixture: ComponentFixture<CountryCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CountryCardComponent, PopulationPipe],
      providers: [ReversePipe]
    });
    fixture = TestBed.createComponent(CountryCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
