import { Component } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss']
})
export class ReactiveComponent {

  myForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.myForm = this.fb.group({
      name: ['', Validators.required, this.nameMatchesEmail.bind(this)],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, this.passwordValidator.bind(this)]],
    });
  }

  onSubmit() {
    // Lógica para manejar la submisión del formulario
    console.log(this.myForm.value);
  }

  nameMatchesEmail(control: AbstractControl): Promise<boolean | null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === this.myForm.get(['email'])?.value.split('@')[0]) {
          resolve(true);
        }
        else {
          resolve(null);
        }
      }
        , 1000);
    });
  }

  passwordValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const password = control.value;
    const hasNumber = /\d/.test(password);
    const hasUpper = /[A-Z]/.test(password);
    const hasLower = /[a-z]/.test(password);
    const valid = hasNumber && hasUpper && hasLower;
    if (!valid) {
      return { invalidPassword: true };
    }
    return null;
  }
        
}

