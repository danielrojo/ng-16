import { Component, Input } from '@angular/core';
import { Card } from 'src/app/models/poker/card';

enum suits {
  spades = '♠',
  hearts = '♥',
  diamonds = '♦',
  clubs = '♣'
}

@Component({
  selector: 'app-poker-card',
  templateUrl: './poker-card.component.html',
  styleUrls: ['./poker-card.component.scss']
})
export class PokerCardComponent {

  mySuits = suits;

  @Input() card!: Card;
  @Input() show: boolean = true;

  constructor() { }

  print() {
    console.log(this.card);
  }


}
