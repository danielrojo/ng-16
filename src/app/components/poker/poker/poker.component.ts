import { AfterViewChecked, AfterViewInit, Component, OnChanges, OnInit, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { PokerService } from 'src/app/services/poker.service';
import {} from 'src/app/models/poker/card';
import { Card } from 'src/app/models/poker/card';
import { PokerCardComponent } from '../poker-card/poker-card.component';


@Component({
  selector: 'app-poker',
  templateUrl: './poker.component.html',
  styleUrls: ['./poker.component.scss']
})
export class PokerComponent implements AfterViewChecked {

  playerCards: Card[] = [];
  dealerCards: Card[] = [];
  show = false;


  @ViewChildren(PokerCardComponent, {read: PokerCardComponent}) children!: PokerCardComponent[];

  constructor(private service: PokerService) { }

  ngAfterViewChecked(): void {
    console.log(this.children?.forEach((child) => {
      console.log(child.print());
    }));
  }

  ngOnInit(): void {
    this.service.playerCards$.subscribe(cards => this.playerCards = cards);
    this.service.dealerCards$.subscribe(cards => this.dealerCards = cards);
    this.service.resolve$.subscribe(resolve => {
      this.show = true;
      console.log(resolve);
      
    })

  }

  dealCard() {
    this.service.deal();


  }


}
