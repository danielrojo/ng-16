export class Country {
    private _name = 'NO NAME';
    private _capital = 'NO CAPITAL';
    private _region = 'NO REGION';
    private _subregion = 'NO SUBREGION';
    private _continent = 'NO CONTINENT';
    private _currencies = new Map<string, any>();
    private _population = -1;
    private _area = -1;
    private _density = -1;
    private _timezones: string[] = [];
    private _borders: string[] = [];
    private _flag = 'NO FLAG';

    constructor(json?: any) {
        
        if (json === undefined) return;

        this._name = json.name.common;
        this.setCapital(json.capital);
        this._region = json.region;
        this._subregion = json.subregion;
        this._continent = json.continents[0];
        this._currencies = json.currencies;
        this._population = json.population;
        this._area = json.area;
        this._timezones = json.timezones;
        this._borders = json.borders;
        this._flag = json.flags.png;


        this.setDensity();
    }

    get name(): string {
        return this._name;
    }

    get capital(): string {
        return this._capital;
    }

    get region(): string {
        return this._region;
    }

    get subregion(): string {
        return this._subregion;
    }

    get continent(): string {
        return this._continent;
    }

    get currencies(): Map<string, any> {
        return this._currencies;
    }

    get population(): number {
        return this._population;
    }

    get area(): number {
        return this._area;
    }

    get timezones(): string[] {
        return this._timezones;
    }

    get borders(): string[] {
        return this._borders;
    }

    get flag(): string {
        return this._flag;
    }

    get density(): number {
        return this._density;
    }

    setDensity(): void {
        this._area === 0 ? this._density = 0 : this._density = this._population / this._area;
    }

    setCapital(capital: string[]): void {
        if (capital === undefined) {
            this._capital = 'NO CAPITAL';            
        } else {
            if (capital.length === 0) {
                this._capital = 'NO CAPITAL';
            } else if (capital.length === 1) {
                this._capital = capital[0];
            } else {
                this._capital = capital[0] + ' (+' + (capital.length - 1) + ')';
            }
        }
    }

}
