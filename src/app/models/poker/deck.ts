import { Card, Suit, Value } from './card';

export class Deck {

        private _deckCards: Card[] = [];
        private _outCards: Card[] = [];

        constructor() {
            this._deckCards = [];
            this._deckCards = [];
            for (let suit of Object.values(Suit)) {
                for (let value of Object.values(Value)) {
                    this._deckCards.push(new Card(value, suit));
                }
            }
        }

        get deckCards() {
            return this._deckCards;
        }

        get outCards() {
            return this._outCards;
        }

        shuffle() {
            for (let i = this._deckCards.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [this._deckCards[i], this._deckCards[j]] = [this._deckCards[j], this._deckCards[i]];
            }
        }

        deal(): Card | undefined {
            if (this._deckCards.length == 0) {
                return undefined;
            }
            this._outCards.push(this._deckCards[this._deckCards.length - 1]);
            return this._deckCards.pop();
        }
    }
