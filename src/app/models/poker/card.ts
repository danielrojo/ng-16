    export enum Value {
        ace = 'A',
        two = '2',
        three = '3',
        four = '4',
        five = '5',
        six = '6',
        seven = '7',
        eight = '8',
        nine = '9',
        ten = '10',
        jack = 'J',
        queen = 'Q',
        king = 'K'
    }
    
    export enum Suit {
        spades = '♠',
        hearts = '♥',
        diamonds = '♦',
        clubs = '♣'
    }
    
    export class Card {

        constructor(private _value: Value, private _suit: Suit) { }
    
        get value(): string {
            return this._value;
        }
    
        get suit(): string {
            return this._suit;
        }
    }

