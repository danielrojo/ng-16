
import { Card } from "./card";
import { Deck } from "./deck";
import { PokerInterpreter } from "./pokerinterpreter";
import { PokerState, PokerStateMachine } from "./pokerstatemachine";


export class PokerFacade {

    private _deck: Deck = new Deck();

    private _playerCards: Card[] = [];
    private _dealerCards: Card[] = [];
    private _playerWins!:boolean;
    private _interpreter = new PokerInterpreter(this._playerCards, this._dealerCards)

    private _pokerStateMachine = new PokerStateMachine();

    constructor() {
        this._initDeck();
    }

    get playerWins(){
        return this._interpreter.playerWins();
    }

    private _initDeck(): void {
        this._deck = new Deck();
        this._deck.shuffle();
    }

    init() {
        this._pokerStateMachine.startGame();
        // deal 3 cards to player and 3 cards to dealer
        for (let i = 0; i < 3; i++) {
            this._playerCards.push(this._deck.deal()!);
            this._dealerCards.push(this._deck.deal()!);
        }
        return [this._playerCards, this._dealerCards];

    }

    deal() {
        switch (this._pokerStateMachine.state) {
            case PokerState.INIT:
                return this.init();
            case PokerState.INGAME:
                let state = this._pokerStateMachine.drawCard();
                if (state == PokerState.INGAME) {
                    this._playerCards.push(this._deck.deal()!);
                    this._dealerCards.push(this._deck.deal()!);
                    return [this._playerCards, this._dealerCards];
                } else {
                    // show cards
                    return undefined;
                }
            case PokerState.SHOW:
                this._dealerCards = [];
                this._playerCards = [];
                return this.init();
            default:
                return undefined;
        }
    }


}
