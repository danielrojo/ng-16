export enum PokerState {
    INIT,
    INGAME,
    SHOW
}

export class PokerStateMachine {

    private _state: PokerState = PokerState.INIT;
    private _currentCards: number = 0;

    constructor() {
    }

    get state() {
        return this._state;
    }

    startGame(): PokerState {
        this._state = PokerState.INGAME;
        this._currentCards = 3;
        return this._state;
    }

    drawCard(): PokerState {
        if (this._currentCards == 5) {
            this._state = PokerState.SHOW
        } else {
            this._currentCards++;
        }
        return this._state;
    }

}