import { Card } from './card';

export class PokerInterpreter {
    private _playerPoints: number = 0;
    private _dealerPoints: number = 0;
    private _playerCards: Card[] = [];
    private _dealerCards: Card[] = [];
    constructor(playerCards: Card[], dealerCards: Card[]) {
        if (playerCards.length != 5 || dealerCards.length != 5) {
            console.log("Error: player or dealer cards are not 5");
            
        }else {
            this._playerCards = playerCards;
            this._dealerCards = dealerCards;
        }
    }

    _pair(cards: Card[]): number{
        let pair = 0;
        for(let i = 0; i < cards.length; i++){
            for(let j = i+1; j < cards.length; j++){
                if(cards[i].value == cards[j].value){
                    pair++;
                }
            }
        }
        return pair;
    }

    _twoPair(cards: Card[]): number{
        let pair = 0;
        for(let i = 0; i < cards.length; i++){
            for(let j = i+1; j < cards.length; j++){
                if(cards[i].value == cards[j].value){
                    pair++;
                }
            }
        }
        if(pair == 2){
            return 1;
        }
        return 0;
    }

    _threeOfAKind(cards: Card[]): number{
        let threeOfAKind = 0;
        for(let i = 0; i < cards.length; i++){
            for(let j = i+1; j < cards.length; j++){
                for(let k = j+1; k < cards.length; k++){
                    if(cards[i].value == cards[j].value && cards[i].value == cards[k].value){
                        threeOfAKind++;
                    }
                }
            }
        }
        return threeOfAKind;
    }

    _straight(cards: Card[]): number{
        let straight = 0;
        let cardValues:string[] = [];
        for(let i = 0; i < cards.length; i++){
            cardValues.push(cards[i].value);
        }
        cardValues.sort();
        for(let i = 0; i < cardValues.length-1; i++){
            if(cardValues[i+1] == cardValues[i]+1){
                straight++;
            }
        }
        return straight;
    }

    _flush(cards: Card[]): number{
        let flush = 0;
        for(let i = 0; i < cards.length-1; i++){
            if(cards[i].suit == cards[i+1].suit){
                flush++;
            }
        }
        return flush;
    }

    _fullHouse(cards: Card[]): number{
        let fullHouse = 0;
        for(let i = 0; i < cards.length; i++){
            for(let j = i+1; j < cards.length; j++){
                for(let k = j+1; k < cards.length; k++){
                    if(cards[i].value == cards[j].value && cards[i].value == cards[k].value){
                        fullHouse++;
                    }
                }
            }
        }
        if(fullHouse == 1){
            return 1;
        }
        return 0;
    }

    _fourOfAKind(cards: Card[]): number{
        let fourOfAKind = 0;
        for(let i = 0; i < cards.length; i++){
            for(let j = i+1; j < cards.length; j++){
                for(let k = j+1; k < cards.length; k++){
                    for(let l = k+1; l < cards.length; l++){
                        if(cards[i].value == cards[j].value && cards[i].value == cards[k].value && cards[i].value == cards[l].value){
                            fourOfAKind++;
                        }
                    }
                }
            }
        }
        return fourOfAKind;
    }

    _straightFlush(cards: Card[]): number{
        let straightFlush = 0;
        let cardValues:string[] = [];
        for(let i = 0; i < cards.length; i++){
            cardValues.push(cards[i].value);
        }
        cardValues.sort();
        for(let i = 0; i < cardValues.length-1; i++){
            if(cardValues[i+1] == cardValues[i]+1){
                straightFlush++;
            }
        }
        for(let i = 0; i < cards.length-1; i++){
            if(cards[i].suit == cards[i+1].suit){
                straightFlush++;
            }
        }
        return straightFlush;
    }

    // this method should return the player's hand value
    // pair = 0.1, two pair = 0.2, three of a kind = 0.3, straight = 4, flush = 5, full house = 6, four of a kind = 7, straight flush = 8
    _evaluateHand(): void{
        this._playerPoints = 0.1*this._pair(this._playerCards) + 0.2*this._twoPair(this._playerCards) + 0.3*this._threeOfAKind(this._playerCards) + 4*this._straight(this._playerCards) + 5*this._flush(this._playerCards) + 6*this._fullHouse(this._playerCards) + 7*this._fourOfAKind(this._playerCards) + 8*this._straightFlush(this._playerCards);
        this._dealerPoints = 0.1*this._pair(this._dealerCards) + 0.2*this._twoPair(this._dealerCards) + 0.3*this._threeOfAKind(this._dealerCards) + 4*this._straight(this._dealerCards) + 5*this._flush(this._dealerCards) + 6*this._fullHouse(this._dealerCards) + 7*this._fourOfAKind(this._dealerCards) + 8*this._straightFlush(this._dealerCards);
    }

    playerWins(): boolean{
        this._evaluateHand();
        if(this._playerPoints > this._dealerPoints){
            return true;
        }
        return false;
    }

}