import { TetrisPiece } from './tetrispiece';

class BuilderTetris {
    constructor() {}

    public build(shape: number[][], color: string, type: string): TetrisPiece {
        return new TetrisPiece(shape, color, type);
    }
}

const builderTetris = new BuilderTetris();
const shape = [
    [1, 0, 1, 0],
    [1, 0, 1, 0], 
    [1, 1, 1, 0], 
    [0, 0, 0, 0]
];
const color = "#44A500";
const type = "U";
const lPiece = builderTetris.build(shape, color, type);