import { Piece, TetrisType } from "./tetris";
import { IPiece } from "./Ipiece";
import { LPiece } from "./Lpiece";

class FactoryTetris {

    getTetrisPiece(value: TetrisType): Piece {
        switch (value) {
            case TetrisType.I:
                return new IPiece();
            case TetrisType.L:
                return new LPiece();
            default:
                throw new Error("Invalid Tetris Type");
        }
    }
}

const factory = new FactoryTetris();
const piece = factory.getTetrisPiece(TetrisType.I);
piece.rotate();