export class TetrisPiece {
    private _shape!: number[][];
    private _type!: string;
    private _color!: string;
    private _rotation!: number;

    constructor(shape: number[][], type: string, color: string) {
        this.shape = shape;
        this.type = type;
        this.color = color;
        this.rotation = 0;
    }

    get shape(): number[][] {
        return this._shape;
    }

    set shape(shape: number[][]) {
        // check if shape is valid
        if (shape.length != 4 || shape[0].length != 4) {
            throw new Error("Invalid shape");
        }
        // check that matrix only contains 0s and 1s
        for (let i = 0; i < shape.length; i++) {
            for (let j = 0; j < shape[i].length; j++) {
                if (shape[i][j] != 0 && shape[i][j] != 1) {
                    throw new Error("Invalid shape");
                }
            }
        }
        this._shape = shape;
    }

    get type(): string {
        return this._type;
    }

    set type(type: string) {
        this._type = type;
    }

    get color(): string {
        return this._color;
    }

    set color(color: string) {
        this._color = color;
    }

    get rotation(): number {
        return this._rotation;
    }

    set rotation(rotation: number) {
        this._rotation = rotation;
    }


}