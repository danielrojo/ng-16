import { Piece, TetrisType, IRotations, TetrisColor } from './tetris';

export class IPiece extends Piece {

    constructor() {
        super();
        this.shape = IRotations[0];
        this.type = TetrisType.I;
        this.color = TetrisColor.I;
    }

    public move(): void {
        console.log("Moving I piece");
    }

    public rotate(): void {
        console.log("Rotating I piece");
        this.rotation = (this.rotation + 1) % 2;
        this.shape = IRotations[this.rotation];
    }
}
