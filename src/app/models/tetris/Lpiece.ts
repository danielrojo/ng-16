import { Piece, TetrisType, LRotations, TetrisColor } from './tetris';

export class LPiece extends Piece {

    constructor() {
        super();
        this.shape = LRotations[0];
        this.type = TetrisType.L;
        this.color = TetrisColor.L;
    }

    public move(): void {
        console.log("Moving L piece");
    }

    public rotate(): void {
        console.log("Rotating L piece");
        this.rotation = (this.rotation + 1) % 4;
        this.shape = LRotations[this.rotation];
    }
    
}

const lPiece = new LPiece();
lPiece.rotate();

