export enum TetrisType {
    I, J, L, O, S, T, Z
}

export enum TetrisColor {
    I = "#00FFFF", // Cyan
    J = "#0000FF", // Blue
    L = "#FFA500", // Orange
    O = "#FFFF00", // Yellow
    S = "#00FF00", // Green
    T = "#800080", // Purple
    Z = "#FF0000"  // Red
}

export abstract class Piece {
    public shape!: number[][];
    public type!: TetrisType;
    public color!: string;
    public rotation = 0;

    public abstract move(): void;
    public abstract rotate(): void;
}

export const LRotations: number[][][] = [
    [[1, 0, 0, 0],
     [1, 0, 0, 0],
     [1, 1, 0, 0],
     [0, 0, 0, 0]
    ],
    [[1, 1, 1, 0],
     [1, 0, 0, 0],
     [0, 0, 0, 0],
     [0, 0, 0, 0]
    ],
    [[1, 1, 0, 0],
     [0, 1, 0, 0],
     [0, 1, 0, 0],
     [0, 0, 0, 0]
    ],
    [[0, 0, 1, 0],
     [1, 1, 1, 0],
     [0, 0, 0, 0],
     [0, 0, 0, 0]
    ]
];

export const IRotations: number[][][] = [
    [[0, 0, 0, 0],
     [1, 1, 1, 1],
     [0, 0, 0, 0],
     [0, 0, 0, 0]
    ],
    [[0, 1, 0, 0],
     [0, 1, 0, 0],
     [0, 1, 0, 0],
     [0, 1, 0, 0]
    ]
];


