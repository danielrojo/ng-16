import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'population'
})
export class PopulationPipe implements PipeTransform {

  transform(value: number, ...args: string[]): string {    
    // return value in format x.xxM
    if (value >= 10000) {
      return `${(value / 1000000).toFixed(2)}M`;
    }else if (value >= 1000) {
      return `${(value / 1000).toFixed(2)}K`;
    }else if (value >= 0) {
      return `${value}`;
    }else {
      console.log('invalid population!!');
      return `${value}`;
    }
  }

}
