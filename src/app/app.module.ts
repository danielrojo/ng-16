import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CountriesComponent } from './components/countries/countries/countries.component';
import { CountriesService } from './services/countries.service';

import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountryCardComponent } from './components/countries/country-card/country-card.component';
import { PopulationPipe } from './pipes/population.pipe';
import { NgPipesModule } from 'ngx-pipes';
import { ReactiveComponent } from './components/forms/reactive/reactive.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PokerComponent } from './components/poker/poker/poker.component';
import { PokerCardComponent } from './components/poker/poker-card/poker-card.component';

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CountryCardComponent,
    PopulationPipe,
    ReactiveComponent,
    PokerComponent,
    PokerCardComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, NgbModule, FormsModule, NgPipesModule, AppRoutingModule, ReactiveFormsModule
  ],
  providers: [CountriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
