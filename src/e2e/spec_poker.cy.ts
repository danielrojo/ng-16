describe('Check that poker game works fine', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200/poker')
  });

  it('route works', () => {
    cy.get('#poker-container').should('exist')
  })
  it('first click on button should show 3 cards', () => {
    cy.get('#deal').click()
    cy.get('.card').should('have.length', 3)
  });

  it('second click on button should show 4 cards', () => {
    cy.get('#deal').click()
    cy.get('#deal').click()
    cy.get('.card').should('have.length', 4)
  });

  it('third click on button should show 5 cards', () => {
    cy.get('#deal').click()
    cy.get('#deal').click()
    cy.get('#deal').click()
    cy.get('.card').should('have.length', 5)
  });
})